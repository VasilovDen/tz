from django.db import models
class Person(models.Model):

    name = models.CharField(unique=True, max_length=15)
    processed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name