from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from persons.models import Person
from session.models import PersonControl


def get_next_person(request):
    persons = Person.objects.filter(processed=False)
    try:
        persons_controller = PersonControl.objects.get(id=1)
    except ObjectDoesNotExist:
        return HttpResponse("Some problems.. Do you setup a database?")

    if not persons:
        return HttpResponse("All persons are processed")

    if persons_controller.access_number > len(persons)-1:
        persons_controller.access_number = 0

    person = persons[persons_controller.access_number]
    persons_controller.access_number += 1
    persons_controller.save()
    result = "ID: {}, Name: {}".format(person.id, person.name)
    return HttpResponse(result)


def set_person_as_processed(request, *args):
    try:
        person_id = int(args[0])
        processed = request.REQUEST['processed']
        person = Person.objects.get(id=person_id)
        person.processed = processed
        person.save()
    except (ObjectDoesNotExist, ValueError, KeyError):
        return HttpResponse("0")
    return HttpResponse("1")