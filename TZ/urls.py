from django.conf.urls import patterns, include, url
from django.contrib import admin
from persons.views import get_next_person, set_person_as_processed

urlpatterns = patterns('',
    url(r'^$', get_next_person),
    url(r'^([0-9]{1,})(processed)?', set_person_as_processed),
    url(r'^admin/', include(admin.site.urls)),
)
